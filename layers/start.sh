#!/bin/sh
docker build . -t layer
docker run --rm layer
docker history layer >> history.txt
docker save layer > layer.tar

