#!/bin/sh

if [ $# -lt 1 ] ; then
	echo "Usage: ./start.sh <PORT>"
	exit 1
fi

TEST_PORT=$1

docker build . -t ej-nginx
docker run --name some-nginx -p 8080:80 -d ej-nginx 
